
var charGroup;
var ground;
var groundArray;
var gravity = 500;
var groundSize = 16;

var XTILEWIDTH = 27;
var WORLD_TILE_HEIGHT = 1024;

var LAVA_BASE_DAMAGE = -20;

var deathReason = "You died due to pure incompetency.";

var deathReasons = {
    lava: "Your miner burned up in the lava",
    falling: "Your miner was smashed into smithereens",
    fuel: "Your miner ran out of fuel and exploded",
}


var oreSpriteSheet;
var tileIndicesToNames = { 0: 'empty', 1: 'grass', 2: 'dirt', 3: 'iron', 4: 'copper', 5: 'silver' , 6: 'gold', 7: 'vibranium', 8: 'grunium', 9: 'lava'};
var tileNamesToIndices = { empty: 0, grass: 1, dirt: 2, iron: 3, copper: 4, silver: 5, gold: 6, vibranium: 7, grunium: 8, lava: 9 };

var oreIndicesToNames = { 3: 'iron', 4: 'copper', 5: 'silver', 6: 'gold', 7: 'vibranium', 8: 'grunium'};
var oreNamesToIndices = { iron: 3, copper: 4, silver: 5, gold: 6, vibranium: 7, grunium: 8};

var orePositions = { iron: 0, copper: 1, silver: 2, gold: 3, vibranium: 4, grunium: 5 };

var orePriceDict = {
    iron:   20,
    copper: 50,
    silver: 100,
    gold:   200,
    vibranium: 500,
    grunium: 1000,
}

// =============================== PLAYER DATA ================================= //

upgrades = {
    fuelTank    : [ { level: 0, type: "basic", value: 30000 }, { level: 1, type: "better", value: 60 }, { level: 2, type: "best", value: 120 }, { level: 3, type: "best", value: 1200 }, { level: 4, type: "best", value: 2400 }, { level: 5, type: "best", value: 3200 } ],
    hull        : [ { level: 0, type: "basic", value: 60 }, { level: 1, type: "copper", value: 100 }, { level: 2, type: "silver", value: 160 }, { level: 3, type: "gold", value: 240 }, { level: 4, type: "vibranium", value: 330 }, { level: 5, type: "grunium", value: 500 } ],
    drill       : [ { level: 0, type: "iron", value: 1200 }, { level: 1, type: "copper", value: 800 }, { level: 2, type: "silver", value: 450 }, { level: 3, type: "gold", value: 300 }, { level: 4, type: "vibranium", value: 200 }, { level: 5, type: "grunium", value: 150 } ],
    cargoBay    : [ { level: 0, type: "basic", value: 20 }, { level: 1, type: "copper", value: 50 }, { level: 2, type: "silver", value: 100 }, { level: 3, type: "gold", value: 150 }, { level: 4, type: "vibranium", value: 300 }, { level: 5, type: "grunium", value: 500 } ],
    engine      : [ { level: 0, type: "basic", value: { xAcc:15, xSpeed: 400, y:15, sound: 'sfxBoosters1'} }, { level: 1, type: "better", value: { xAcc:35, xSpeed: 600, y:20, sound: 'sfxBoosters2'} }, { level: 2, type: "best", value: { xAcc:50, xSpeed: 800, y:25, sound: 'sfxBoosters3'} } ],
    radiator    : [ { level: 0, type: "basic", value: 1 }, { level: 1, type: "better", value: 0.75 }, { level: 2, type: "best", value: 0.5 } ]
}

var currentUpgrades = {
    fuelTank: upgrades.fuelTank[0],
    hull: upgrades.hull[0],
    drill: upgrades.drill[0],
    cargoBay: upgrades.cargoBay[0],
    engine: upgrades.engine[0],
    radiator: upgrades.radiator[0],
}

var minerPlacementHack = false;

function setMinerPlacementHack() {
    miner.body.collideWorldBounds = true;
    minerPlacementHack = true;
}

var maxHealth;
var health;
var healthUI;

var fuelTimer;
var maxFuel;
var fuel;
var fuelUI;

var money;
var moneyUI;

var inventorySize;
var inventory;
var inventoryUI;

var bankAnimTimer;

function upgradePart(part, level) {
    // var upgradeIndex = upgrades[part].indexOf(currentUpgrades[part]);
    var upgradeIndex = currentUpgrades[part].level;
    var beforeUpgrade = currentUpgrades[part];

    // Set the desired part to the desired level.
    upgradeIndex = level;
    currentUpgrades[part] = upgrades[part][upgradeIndex];
    console.log(beforeUpgrade.type + " " + part + "(level " + beforeUpgrade.level + ") upgraded to " + currentUpgrades[part].type + " " + part  + "(level " + currentUpgrades[part].level + ").");

    if (part === "drill") {
        drillAnimIndex = level;
    }

    if (part === "hull") {
        minerBody.frame = level;
        setMaxHealth();
    }

    if (part === "engine") {
        boosterAnimIndex = level;
        sfxBoosters.key = currentUpgrades["engine"].value.sound;
        sfxBoosters.restart("", 0, 0, true);
    }

    if (part === "fuelTank") {
        maxFuel = currentUpgrades['fuelTank'].value;
        refuel();
    }

    if (part === "cargoBay") {
        inventorySize = currentUpgrades['cargoBay'].value;
        inventoryUI.text = getInventoryUIString();
    }
}


// ============================================================================= //

var drillPositions = { left: { x: -45, y: 3, angle: 0}, right: { x: 45, y: 3, angle: 180}, down: { x: 0, y: 40, angle: -90}};

var playState = {


    create: function() {

        game.plugins.add(Phaser.Plugin.PhaserIlluminated);
        game.time.advancedTiming = true;

        game.desiredFPS = 60;

        game.world.setBounds(0, -1000, XTILEWIDTH * oreSize, WORLD_TILE_HEIGHT * oreSize);

        game.physics.startSystem(Phaser.Physics.ARCADE);
        // game.physics.arcade.TILE_BIAS = 40;
        game.physics.arcade.gravity.y = gravity;
        game.stage.backgroundColor = '#124184';

        cursors = { up:    game.input.keyboard.addKey(Phaser.Keyboard.W),
                    left:  game.input.keyboard.addKey(Phaser.Keyboard.A),
                    down:  game.input.keyboard.addKey(Phaser.Keyboard.S),
                    right: game.input.keyboard.addKey(Phaser.Keyboard.D)
                };

        refuelKey = game.input.keyboard.addKey(Phaser.Keyboard.F);
        refuelKey.onDown.add(refuel, this);

        repairKey = game.input.keyboard.addKey(Phaser.Keyboard.R);
        repairKey.onDown.add(repairFull, this);

        dynamiteKey = game.input.keyboard.addKey(Phaser.Keyboard.H);
        dynamiteKey.onDown.add(useDynamite, this);
        // bg = game.add.tileSprite(game.world.centerX, game.world.centerY, 4000, 4000, 'skyBG');
        // bg.anchor.set(0.5);


        // ================================= TILES ==================================

        // bank = game.add.sprite(450, 80, 'dirt');
        // bank.anchor.setTo(0.5,0.5);
        // game.physics.enable(bank, Phaser.Physics.ARCADE);
        // bank.body.moves = false;

        var groundMap = drawGround();


        //  Add data to the cache
        game.cache.addTilemap('dynamicMap', null, groundMap, Phaser.Tilemap.CSV);

        //  Create our map (the oreSizexoreSize is the tile size)
        map = game.add.tilemap('dynamicMap', oreSize, oreSize);

        //  'tiles' = cache image key, oreSizexoreSize = tile size
        map.addTilesetImage('oreSpriteSheet', 'oreSpriteSheet', oreSize, oreSize);
        //  0 is important
        groundLayer = map.createLayer(0);
        map.setCollisionBetween(1, 9, true, groundLayer, true);
        // map.setTileIndexCallback([1,2,3,4], this);


        for (var y = 0; y < map.height; y++) {
            for (var x = 0; x < map.width; x++) {
                var tile = map.getTile(x,y);
                // tile.roughTop = game.add.sprite(tile.x,tile.y,'dirtRoughTop');
                // console.log(tile);
            }
        }

        //  Scroll it
        // groundLayer.resizeWorld();


        // background sprites
        portalGate = game.add.sprite(1200, -170, 'portalGate');
        portalGate.anchor.setTo(0.5,0.5);
        game.physics.enable(portalGate, Phaser.Physics.ARCADE);
        portalGate.body.moves = false;

        portalBaseCollision = game.add.sprite(portalGate.x, portalGate.y + 157);
        portalBaseCollision.anchor.setTo(0.5,0.5);
        portalBaseCollision.width = portalGate.width;
        portalBaseCollision.height = 48;
        game.physics.enable(portalBaseCollision, Phaser.Physics.ARCADE);
        portalBaseCollision.body.moves = false;
        portalBaseCollision.body.immovable = true;

        bankPortal = game.add.sprite(portalGate.x, portalGate.y, 'portalSpriteSheet');
        game.physics.enable(bankPortal, Phaser.Physics.ARCADE);
        bankPortal.body.moves = false;
        bankPortal.anchor.setTo(0.5,0.5);
        bankPortal.scale.setTo(0,0);
        bankPortal.visible = false;
        bankPortal.animations.add('bankPortalAnim');

        bankPortalEmitter = game.add.emitter(portalGate.x, portalGate.y - 20, 200);
        bankPortalEmitter.makeParticles('portalParticle');
        bankPortalEmitter.minParticleSpeed.setTo(-400, -400);
        bankPortalEmitter.maxParticleSpeed.setTo(400, 400);
        bankPortalEmitter.gravity = -400;
        bankPortalEmitter.bounce.setTo(0.3, 0.3);
        bankPortalEmitter.angularDrag = 30;

        bankDetector = game.add.sprite(portalGate.x, portalGate.y - 20);
        bankDetector.anchor.setTo(0.5,0.5);
        bankDetector.width = portalGate.width - 100;
        bankDetector.height = portalGate.height - 130;
        game.physics.enable(bankDetector, Phaser.Physics.ARCADE);
        bankDetector.body.moves = false;
        bankDetector.kill();

        bankEmitters = {
            // dirt :  { emitter: game.add.emitter(bankDetector.x, bankDetector.y, 40,), timer: game.time.create() },
            iron:       { emitter: game.add.emitter(bankDetector.x, bankDetector.y, 40,), timer: game.time.create() }, // 250 might need looking at, meant to be max particles per emitter.
            copper:     { emitter: game.add.emitter(bankDetector.x, bankDetector.y, 40,), timer: game.time.create() }, // 250 might need looking at, meant to be max particles per emitter.
            silver:     { emitter: game.add.emitter(bankDetector.x, bankDetector.y, 40,), timer: game.time.create() }, // 250 might need looking at, meant to be max particles per emitter.
            gold:       { emitter: game.add.emitter(bankDetector.x, bankDetector.y, 40,), timer: game.time.create() }, // 250 might need looking at, meant to be max particles per emitter.
            vibranium:  { emitter: game.add.emitter(bankDetector.x, bankDetector.y, 40,), timer: game.time.create() },
            grunium:    { emitter: game.add.emitter(bankDetector.x, bankDetector.y, 40,), timer: game.time.create() },
        }

        for (var em in bankEmitters) {
            bankEmitters[em].emitter.makeParticles('oreParticleSpriteSheet', orePositions[em], 40, true, true); // set the correct resource for each one.
            bankEmitters[em].emitter.minParticleSpeed.setTo(-130, -30);
            bankEmitters[em].emitter.maxParticleSpeed.setTo(130, 100);
            bankEmitters[em].emitter.gravity = -400;
            bankEmitters[em].emitter.bounce.setTo(0.3, 0.3);
            bankEmitters[em].emitter.angularDrag = 30;
        }


        // ============== REPAIR STATION ======================
        repairStation = game.add.sprite(550, -120, "repairStation");
        repairDroid = repairStation.addChild(game.make.sprite(52,63, 'repairDroid'));
        repairDroid.animations.add('repairDroidIdleAnim');
        repairDroid.animations.play('repairDroidIdleAnim', 3, true);

        repairStationRoofCollider = repairStation.addChild(game.make.sprite(57, 0));
        repairStationRoofCollider.width = 193;
        repairStationRoofCollider.height = 38;
        game.physics.enable(repairStationRoofCollider, Phaser.Physics.ARCADE);
        repairStationRoofCollider.body.moves = false;
        repairStationRoofCollider.body.immovable = true;

        var repairMoney1 = {x:185,y:60, text: "£5"};
        var repairMoney2 = {x:227,y:60, text: "£10"};  // 42 x difference, y value for second row is -> 25 so y=87
        var repairMoney3 = {x:185,y:85, text: "£20"};
        var repairMoney4 = {x:227,y:85, text: "£50"};

        repairStation.addChild(game.make.text(repairMoney1.x + 7, repairMoney1.y + 2, repairMoney1.text, { font: "14px Dosis", fill: '#FFC859', boundsAlignH: "center", boundsAlignV: "middle"}));
        repairStation.addChild(game.make.text(repairMoney2.x + 7, repairMoney2.y + 2, repairMoney2.text, { font: "14px Dosis", fill: '#FFC859', boundsAlignH: "center", boundsAlignV: "middle"}));
        repairStation.addChild(game.make.text(repairMoney3.x + 7, repairMoney3.y + 2, repairMoney3.text, { font: "14px Dosis", fill: '#FFC859', boundsAlignH: "center", boundsAlignV: "middle"}));
        repairStation.addChild(game.make.text(repairMoney4.x + 7, repairMoney4.y + 2, repairMoney4.text, { font: "14px Dosis", fill: '#FFC859', boundsAlignH: "center", boundsAlignV: "middle"}));

        repairMoneyInput1 = repairStation.addChild(game.make.sprite(repairMoney1.x, repairMoney1.y, "shopSelectionHighlighter"));
        repairMoneyInput1.repairCost = 5;
        repairMoneyInput2 = repairStation.addChild(game.make.sprite(repairMoney2.x, repairMoney2.y, "shopSelectionHighlighter"));
        repairMoneyInput2.repairCost = 10;
        repairMoneyInput3 = repairStation.addChild(game.make.sprite(repairMoney3.x, repairMoney3.y, "shopSelectionHighlighter"));
        repairMoneyInput3.repairCost = 20;
        repairMoneyInput4 = repairStation.addChild(game.make.sprite(repairMoney4.x, repairMoney4.y, "shopSelectionHighlighter"));
        repairMoneyInput4.repairCost = 50;

        var repairMoneyInputBoxes = [repairMoneyInput1, repairMoneyInput2, repairMoneyInput3, repairMoneyInput4];

        for (var box in repairMoneyInputBoxes) {
            repairMoneyInputBoxes[box].inputEnabled = true;
            repairMoneyInputBoxes[box].alpha = 0;
            repairMoneyInputBoxes[box].events.onInputOver.add(mouseOverHighlight, this);
            repairMoneyInputBoxes[box].events.onInputOut.add(mouseOverHighlight, this);
            repairMoneyInputBoxes[box].events.onInputDown.add(buyRepair, this);
        }


        // ================ FUEL STATION ==================
        fuelStation = game.add.sprite(200, -120, "fuelStation");

        fuelStationRoofCollider = fuelStation.addChild(game.make.sprite(110, 0));
        fuelStationRoofCollider.width = 190;
        fuelStationRoofCollider.height = 38;
        game.physics.enable(fuelStationRoofCollider, Phaser.Physics.ARCADE);
        fuelStationRoofCollider.body.moves = false;
        fuelStationRoofCollider.body.immovable = true;

        // game.physics.enable(fuelStation, Phaser.Physics.ARCADE);
        // fuelStation.body.moves = false;
        // fuelStation.body.immovable = true;
        var fuelMoney1 = {x:196,y:60, text: "£5"};
        var fuelMoney2 = {x:238,y:60, text: "£10"};  // 42 x difference, y value for second row is -> 25 so y=87
        var fuelMoney3 = {x:196,y:85, text: "£20"};
        var fuelMoney4 = {x:238,y:85, text: "£50"};

        fuelStation.addChild(game.make.text(fuelMoney1.x + 7, fuelMoney1.y + 2, fuelMoney1.text, { font: "14px Dosis", fill: '#FFC859', boundsAlignH: "center", boundsAlignV: "middle"}));
        fuelStation.addChild(game.make.text(fuelMoney2.x + 7, fuelMoney2.y + 2, fuelMoney2.text, { font: "14px Dosis", fill: '#FFC859', boundsAlignH: "center", boundsAlignV: "middle"}));
        fuelStation.addChild(game.make.text(fuelMoney3.x + 7, fuelMoney3.y + 2, fuelMoney3.text, { font: "14px Dosis", fill: '#FFC859', boundsAlignH: "center", boundsAlignV: "middle"}));
        fuelStation.addChild(game.make.text(fuelMoney4.x + 7, fuelMoney4.y + 2, fuelMoney4.text, { font: "14px Dosis", fill: '#FFC859', boundsAlignH: "center", boundsAlignV: "middle"}));

        fuelMoneyInput1 = fuelStation.addChild(game.make.sprite(fuelMoney1.x, fuelMoney1.y, "shopSelectionHighlighter"));
        fuelMoneyInput1.fuelCost = 5;
        fuelMoneyInput2 = fuelStation.addChild(game.make.sprite(fuelMoney2.x, fuelMoney2.y, "shopSelectionHighlighter"));
        fuelMoneyInput2.fuelCost = 10;
        fuelMoneyInput3 = fuelStation.addChild(game.make.sprite(fuelMoney3.x, fuelMoney3.y, "shopSelectionHighlighter"));
        fuelMoneyInput3.fuelCost = 20;
        fuelMoneyInput4 = fuelStation.addChild(game.make.sprite(fuelMoney4.x, fuelMoney4.y, "shopSelectionHighlighter"));
        fuelMoneyInput4.fuelCost = 50;

        var fuelMoneyInputBoxes = [fuelMoneyInput1, fuelMoneyInput2, fuelMoneyInput3, fuelMoneyInput4];

        for (var box in fuelMoneyInputBoxes) {
            fuelMoneyInputBoxes[box].inputEnabled = true;
            fuelMoneyInputBoxes[box].alpha = 0;
            fuelMoneyInputBoxes[box].events.onInputOver.add(mouseOverHighlight, this);
            fuelMoneyInputBoxes[box].events.onInputOut.add(mouseOverHighlight, this);
            fuelMoneyInputBoxes[box].events.onInputDown.add(buyFuel, this);
        }

        function mouseOverHighlight(sprite) {
            sprite.alpha = sprite.alpha === 0 ? 1 : 0;
        }

        upgradeStation = game.add.sprite(1550, -900, "upgradeStation"); // x = 1600
        upgradeStationFloors = game.add.group();
        for (var i = 0; i < 5; i++) {
            var floor = game.make.sprite(157, 248+(i*165));
            floor.width = 383;
            floor.height = 17;
            game.physics.enable(floor, Phaser.Physics.ARCADE);
            floor.body.moves = false;
            floor.body.immovable = true;

            upgradeStationFloors.add(floor);

        }

        upgradeStation.addChild(upgradeStationFloors);

        var initialX = 110;
        for (var i = 1; i <= 5; i++) {
            var xLoc = initialX + (i * 60);
            var partArtY = 810;
            var drillDisplay = upgradeStation.addChild(game.make.sprite(xLoc, partArtY, "drillUpgrades", i));
            var priceDisplay = upgradeStation.addChild(game.make.sprite(xLoc-5, partArtY + 40, 'upgradePriceDisplay', 0));

            var text = game.make.text(0, 0, "£12000", { font: "13px Dosis", fill: '#FFC859',
                boundsAlignH: "center", boundsAlignV: "middle"});
            text.setTextBounds(0, 0, priceDisplay.width, priceDisplay.height);
            priceDisplay.addChild(text);
            priceDisplay.inputEnabled = true;
            priceDisplay.events.onInputOver.add(function (sprite) { sprite.frame = 1;} );
            priceDisplay.events.onInputOut.add(function (sprite) { sprite.frame = 0;} );
            priceDisplay.events.onInputDown.add( (function (idx) {
                return function() { upgradePart("drill", idx);} })(i) );
        }

        initialX = 115;
        for (var i = 1; i <= 5; i++) {
            var xLoc = initialX + (i * 60);
            var hullDisplay = upgradeStation.addChild(game.make.sprite(xLoc, 650, "hullColourSpriteSheet", i));
            hullDisplay.scale.setTo(0.7,0.7);
            var priceDisplay = upgradeStation.addChild(game.make.sprite(xLoc-5, 695, 'upgradePriceDisplay', 0));

            var text = game.make.text(0, 0, "£12000", { font: "13px Dosis", fill: '#FFC859',
                boundsAlignH: "center", boundsAlignV: "middle"});
            text.setTextBounds(0, 0, priceDisplay.width, priceDisplay.height);
            priceDisplay.addChild(text);
            priceDisplay.inputEnabled = true;
            priceDisplay.events.onInputOver.add(function (sprite) { sprite.frame = 1;} );
            priceDisplay.events.onInputOut.add(function (sprite) { sprite.frame = 0;} );
            priceDisplay.events.onInputDown.add( (function (idx) {
                return function() { upgradePart("hull", idx);} })(i) );
        }

        initialX = 180;
        for (var i = 0; i < 3; i++) {
            var xLoc = initialX + (i * 60);
            var partArtY = 490;
            var engineDisplay = upgradeStation.addChild(game.make.sprite(xLoc, partArtY, "engines", i));
            var priceDisplay = upgradeStation.addChild(game.make.sprite(xLoc-5, partArtY + 45, 'upgradePriceDisplay', 0));

            var text = game.make.text(0, 0, "£12000", { font: "13px Dosis", fill: '#FFC859',
                boundsAlignH: "center", boundsAlignV: "middle"});
            text.setTextBounds(0, 0, priceDisplay.width, priceDisplay.height);
            priceDisplay.addChild(text);
            priceDisplay.inputEnabled = true;
            priceDisplay.events.onInputOver.add(function (sprite) { sprite.frame = 1;} );
            priceDisplay.events.onInputOut.add(function (sprite) { sprite.frame = 0;} );
            priceDisplay.events.onInputDown.add( (function (idx) {
                return function() { upgradePart("engine", idx);} })(i) );
        }

        initialX = 180;
        for (var i = 0; i < 5; i++) {
            var xLoc = initialX + (i * 60);
            var partArtY = 315;
            var fuelDisplay = upgradeStation.addChild(game.make.sprite(xLoc, partArtY, "fuelTanks", i));
            var priceDisplay = upgradeStation.addChild(game.make.sprite(xLoc-5, partArtY + 50, 'upgradePriceDisplay', 0));

            var text = game.make.text(0, 0, "£12000", { font: "13px Dosis", fill: '#FFC859',
                boundsAlignH: "center", boundsAlignV: "middle"});
            text.setTextBounds(0, 0, priceDisplay.width, priceDisplay.height);
            priceDisplay.addChild(text);
            priceDisplay.inputEnabled = true;
            priceDisplay.events.onInputOver.add(function (sprite) { sprite.frame = 1;} );
            priceDisplay.events.onInputOut.add(function (sprite) { sprite.frame = 0;} );
            priceDisplay.events.onInputDown.add( (function (idx) {
                return function() { upgradePart("fuelTank", idx);} })(i) );
        }

        initialX = 110;
        for (var i = 1; i <= 5; i++) {
            var xLoc = initialX + (i * 70);
            var partArtY = 160;
            var fuelDisplay = upgradeStation.addChild(game.make.sprite(xLoc, partArtY, "cargoBays", i));
            var priceDisplay = upgradeStation.addChild(game.make.sprite(xLoc-5, partArtY + 50, 'upgradePriceDisplay', 0));

            var text = game.make.text(0, 0, "£12000", { font: "13px Dosis", fill: '#FFC859',
                boundsAlignH: "center", boundsAlignV: "middle"});
            text.setTextBounds(0, 0, priceDisplay.width, priceDisplay.height);
            priceDisplay.addChild(text);
            priceDisplay.inputEnabled = true;
            priceDisplay.events.onInputOver.add(function (sprite) { sprite.frame = 1;} );
            priceDisplay.events.onInputOut.add(function (sprite) { sprite.frame = 0;} );
            priceDisplay.events.onInputDown.add( (function (idx) {
                return function() { upgradePart("cargoBay", idx);} })(i) );
        }


        // upgradeStation.addChild(game.make.sprite(150, 600, "hullColourSpriteSheet", 2));
        // upgradeStation.addChild(game.make.sprite(200, 600, "hullColourSpriteSheet", 3));
        // upgradeStation.addChild(game.make.sprite(250, 600, "hullColourSpriteSheet", 4));
        // upgradeStation.addChild(game.make.sprite(300, 600, "hullColourSpriteSheet", 5));

        // ================================= END TILES ==================================


        // ================================= CHARACTER ==================================
        // Add a blank sprite as the root of the character. Allows us to have children render below the main body.
        miner = game.add.sprite(game.width / 2, -200, 'nullMiner');
        console.log(miner);
        miner.anchor.setTo(0.5,0.5);

        drill = miner.addChild(game.make.sprite(0, 0, 'drillSpriteSheet'));
        game.physics.enable(drill, Phaser.Physics.ARCADE);
        drill.body.moves = false;
        drill.anchor.setTo(0.5,0.5);
        drill.visible = false;

        ironDrillFrames = Phaser.ArrayUtils.numberArray(0,3);
        copperDrillFrames = Phaser.ArrayUtils.numberArray(4,7);
        silverDrillFrames = Phaser.ArrayUtils.numberArray(8,11);
        goldDrillFrames = Phaser.ArrayUtils.numberArray(12,15);
        vibraniumDrillFrames = Phaser.ArrayUtils.numberArray(16,19);
        gruniumDrillFrames = Phaser.ArrayUtils.numberArray(20,23);

        drill.animations.add('ironExtend', ironDrillFrames, 10, false);
        drill.animations.add('copperExtend', copperDrillFrames, 10, false);
        drill.animations.add('silverExtend', silverDrillFrames, 10, false);
        drill.animations.add('goldExtend', goldDrillFrames, 30, false);
        drill.animations.add('vibraniumExtend', vibraniumDrillFrames, 30, false);
        drill.animations.add('gruniumExtend', gruniumDrillFrames, 30, false);

        drillExtendAnims = ["ironExtend", "copperExtend", "silverExtend", "goldExtend", "vibraniumExtend", "gruniumExtend"];
        drillAnimIndex = 0;

        boosters = [miner.addChild(game.make.sprite(-20, 33, 'boosterSpriteSheet')), miner.addChild(game.make.sprite(20, 33, 'boosterSpriteSheet'))];
        game.physics.enable(boosters[0], Phaser.Physics.ARCADE);
        game.physics.enable(boosters[1], Phaser.Physics.ARCADE);
        boosters[0].body.moves = false;
        boosters[1].body.moves = false;
        boosters[0].anchor.setTo(0.5,0.5);
        boosters[1].anchor.setTo(0.5,0.5);

        basicBoosterFrames = Phaser.ArrayUtils.numberArray(0,2);
        vibraniumBoosterFrames = Phaser.ArrayUtils.numberArray(3,5);
        gruniumBoosterFrames = Phaser.ArrayUtils.numberArray(6,8);

        boosters[0].animations.add('basicFly', basicBoosterFrames);
        boosters[0].animations.add('vibraniumFly', vibraniumBoosterFrames);
        boosters[0].animations.add('gruniumFly', gruniumBoosterFrames);

        boosters[1].animations.add('basicFly', basicBoosterFrames);
        boosters[1].animations.add('vibraniumFly', vibraniumBoosterFrames);
        boosters[1].animations.add('gruniumFly', gruniumBoosterFrames);

        boosterAnims = ["basicFly", "vibraniumFly", "gruniumFly"];
        boosterAnimIndex = 0;

        // var fly = boosters[0].animations.add('fly');
        // boosters[0].animations.play('fly', 30, true);

        // fly = boosters[1].animations.add('fly');
        // boosters[1].animations.play('fly', 30, true);

        // charGroup.add(miner);
        // charGroup.add(drill);

        // Add actual miner body sprite.
        minerBody = miner.addChild(game.make.sprite(0, 0, 'hullColourSpriteSheet'));
        game.physics.enable(minerBody, Phaser.Physics.ARCADE);
        minerBody.body.moves = false;
        minerBody.anchor.setTo(0.5,0.5);

        miner.inputEnabled = true;
        miner.events.onInputDown.add(toggleInventory);
        // minerBody.events.onInputOut.add(hideInventory);

        game.physics.enable(miner, Phaser.Physics.ARCADE);
        miner.name = 'miner';
        miner.body.collideWorldBounds = false;
        miner.body.bounce.setTo(0, 0.2);
        miner.body.maxVelocity.y = 1000;

        miner.bringToTop();
        game.camera.follow(miner);

        // ================================= END CHARACTER ==================================


        // ================================= EXTRA GRAPHICS ==================================
        //Set up digging particles
        digEmitter = game.add.emitter(game.world.centerX, game.world.centerY, 250);

        digEmitter.makeParticles('dirtParticle');

        digEmitter.minParticleSpeed.setTo(-200, -50);
        digEmitter.maxParticleSpeed.setTo(200, -200);
        digEmitter.minParticleScale = 1;
        digEmitter.maxParticleScale = 3;
        digEmitter.gravity = 150;
        digEmitter.bounce.setTo(0.2, 0.2);
        digEmitter.angularDrag = 30;

        explosion = game.add.sprite(200, 200, 'explosion1');
        explosion.scale.setTo(0,0);
        explosion.anchor.setTo(0.5,0.5);
        explosionTween = game.add.tween(explosion.scale).to({ x: 1, y: 1}, 50, Phaser.Easing.Linear.Out, false);
        fadeTween = game.add.tween(explosion).to( {alpha: 0}, 200, Phaser.Easing.Linear.Out, false);

        explosionTween.chain(fadeTween);
        // explosionTween.start();

        bankAnimTimer = game.time.create();

        // game.add.tileSprite(0, 310, game.world.width, 18, 'grass');

        // ================================= END EXTRA GRAPHICS ==================================


        // ================================= SFX ==================================
        sfxDrill = game.add.audio('sfxDrill1');
        sfxDrill.allowMultiple = true;
        sfxDrill.volume = 0.25;

        sfxDrillKeys = ['sfxDrill1', 'sfxDrill2', 'sfxDrill3', 'sfxDrill4'];

        sfxMinerHit = game.add.audio('sfxMinerHit');
        sfxMinerHit.allowMultiple = true;
        sfxMinerHit.volume = 0.25;

        sfxDeath = game.add.audio('sfxDeath');
        sfxDeath.allowMultiple = true;
        sfxDeath.volume = 0.18;

        sfxEngineStart = game.add.audio('sfxEngineStart');
        sfxEngineStart.allowMultiple = false;
        sfxEngineStart.volume = 0.25;

        sfxEngineRunning = game.add.audio('sfxEngineRunning');
        sfxEngineRunning.allowMultiple = false;
        sfxEngineRunning.volume = 0.05;
        sfxEngineRunning.loop = true;

        sfxBoosters = game.add.audio('sfxBoosters1');
        sfxBoosters.allowMultiple = false;
        sfxBoosters.volume = 0;
        sfxBoosters.loop = true;
        sfxBoosters.play();
        // ================================= END SFX ==================================

        // ============================== LIGHTING ===========================================
        // myLamp1 = game.add.illuminated.lamp(miner.x, miner.y /*,{ illuminated lamp config object }*/);
        // myLamp1.anchor.setTo(0.5,0.5);

        // //add an opaque object.  parameters are (x, y, width, height).
        // //this is not a phaser.sprite object because it's not actually drawn,
        // //except by the lamp.
        // //It's an illuminated.polygonObject instance
        // // myObj = game.add.illuminated.rectangleObject(420, 210, 40, 30);

        // //darkmask is a sprite but takes up the entire game screen, IE WxH.
        // //it cookie-cutters out existing lamp implementations.
        // //it needs a reference to all lamp sprites, but these can be added later
        // myLamps = [myLamp1];
        // myMask = game.add.illuminated.darkMask(myLamps/*, color*/);
        // miner.addChild(myMask);
        // myMask.anchor.setTo(0.5,0.5);
        // //myMask.addLampSprite(myLamp2); <-- alternative to adding at construction time

        // myMask.bringToTop();

        LIGHT_RADIUS = 300;

        shadowTexture = game.add.bitmapData(game.width, game.height);

        // Create an object that will use the bitmap as a texture
        lightSprite = game.add.image(0,0, shadowTexture);
        lightSprite.fixedToCamera = true;

        // Set the blend mode to MULTIPLY. This will darken the colors of
        // everything below this sprite.
        lightSprite.blendMode = Phaser.blendModes.MULTIPLY;

        // ============================== END LIGHTING ===========================================

        // ====================== UI ========================

        damageVignette = game.add.sprite(0, 0, "damageVignette");
        damageVignette.fixedToCamera = true;
        damageVignette.alpha = 0;
        damageVignetteFade = game.add.tween(damageVignette).to( {alpha: 0}, 640, Phaser.Easing.Quadratic.Out, false);

        failUIText = game.add.text(game.width / 2, 100, deathReason, { font: "bold 32px Dosis", fill: '#FF5D5D'} );
        failUIText.fixedToCamera = true;
        failUIText.anchor.setTo(0.5,0);
        failUIText.inputEnabled = true;
        failUIText.visible = false;
        failUIText.events.onInputDown.add(restart);

        inventoryWindow = game.add.sprite(minerBody.x + 80, minerBody.y - 80, 'inventoryWindow');
        inventoryWindow.anchor.setTo(0.5,0.5);
        inventoryWindow.scale.setTo(1,0);
        inventoryWindow.hidden = true;

        inventoryWindow.animations.add('invWindowAnim',  [0,1,2,3,2,1,0], 10, true);
        inventoryWindow.animations.play('invWindowAnim');
        inventoryUI = inventoryWindow.addChild(game.make.text(7, 0, getInventoryUIString(), { font: "16px Dosis", fill: '#EEF6ED'}));
        inventoryUI.anchor.setTo(0.5,0.5);

        var inventoryOpenTween = game.add.tween(inventoryWindow.scale).to({ x:1, y:1}, 120, Phaser.Easing.Exponential.In, false);
        var inventoryCloseTween = game.add.tween(inventoryWindow.scale).to({ x:1, y:0}, 120, Phaser.Easing.Exponential.Out, false);

        function toggleInventory() {
            if (inventoryWindow.hidden) {
                inventoryOpenTween.start();
            } else {
                inventoryCloseTween.start();
            }
            inventoryWindow.hidden = !inventoryWindow.hidden;
        }

        healthUIIcon = game.add.sprite(10,10,'healthIcon');
        healthUIIcon.fixedToCamera = true;
        healthUI = game.add.text(55, 20, getHealthUIString(), { font: "bold 22px Dosis", fill: '#37B758'});
        healthUI.fixedToCamera = true;

        fuelUIIcon = game.add.sprite(10,60,'fuelIcon');
        fuelUIIcon.fixedToCamera = true;
        fuelUI = game.add.text(40, 70, getFuelUIString(), { font: "bold 22px Dosis", fill: '#ffff33'});
        fuelUI.fixedToCamera = true;

        moneyUI = game.add.text(game.width / 2, 20, getMoneyUIString(), { font: "20px Dosis", fill: "#ffff33"});
        moneyUI.fixedToCamera = true;

        restart();

        fuelTimer = game.time.create();
        timerEvent = fuelTimer.loop(Phaser.Timer.SECOND, updateFuel, this);

        // Start the timer
        fuelTimer.start();

        soundUI = game.add.sprite(game.width - 10, 20, 'iconSound' + (soundOn ? "On" : "Off"));
        soundUI.anchor.setTo(1,0);
        soundUI.fixedToCamera = true;
        soundUI.inputEnabled = true;
        soundUI.events.onInputDown.add(toggleSoundIcon);

        fpsText = game.add.text(game.width - 40,5, "0", { font: "bold 12px Dosis", fill: "#ffffff"});
        fpsText.fixedToCamera = true;

        // ===================== END UI =======================
         game.tweens.frameBased = true;
    },

    update: function() {

        if (!minerPlacementHack) {
            setMinerPlacementHack();
        }

        fpsText.text = game.time.fps;
        // myLamp1.x = miner.x;
        // myLamp1.y = miner.y;
        // myLamp1.refresh();
        // myMask.refresh();
        // updateShadowTexture();

        game.physics.arcade.collide(miner, groundLayer, touchingGround);
        game.physics.arcade.collide(digEmitter, groundLayer);
        for (var i in bankEmitters) {
            game.physics.arcade.collide(bankEmitters[i].emitter, groundLayer);
            game.physics.arcade.collide(bankEmitters[i].emitter, portalBaseCollision);
        }

        if (!inventoryWindow.hidden) {
            inventoryWindow.x = miner.x + 110;
            inventoryWindow.y = miner.y - 50;

            if (miner.x > game.world.width - inventoryWindow.width) {
                inventoryWindow.x = miner.x - 130;
            } else {
                inventoryWindow.scale.setTo(1,inventoryWindow.scale.y);
                inventoryWindow.x = miner.x + 102;
            }
        }

        game.physics.arcade.collide(miner, portalBaseCollision);
        game.physics.arcade.collide(miner, upgradeStationFloors);
        game.physics.arcade.collide(miner, fuelStationRoofCollider);
        game.physics.arcade.collide(miner, repairStationRoofCollider);
        game.physics.arcade.overlap(miner, bankDetector, sellResources);
        if (checkOverlap(miner, fuelStation)) {
            fuelTimer.pause();
        } else {
            fuelTimer.resume();
        }

        if (miner.body.velocity.x > 0) {
            miner.body.velocity.x -= 5;
        } else if (miner.body.velocity.x < 0) {
            miner.body.velocity.x += 5;
        }

        // Movement code
        if (cursors.left.isDown && miner.body.velocity.x > -1 * currentUpgrades.engine.value.xSpeed) {
            miner.body.velocity.x -= currentUpgrades.engine.value.xAcc;
        } else if (cursors.right.isDown && miner.body.velocity.x < currentUpgrades.engine.value.xSpeed) {
            miner.body.velocity.x += currentUpgrades.engine.value.xAcc;
        }

        if (cursors.up.isDown && miner.body.velocity.y > -1000) {
            boosters[0].visible = true;
            boosters[1].visible = true;
            boosters[0].animations.play(boosterAnims[boosterAnimIndex], 30, true);
            boosters[1].animations.play(boosterAnims[boosterAnimIndex], 30, true);
            miner.body.velocity.y -= currentUpgrades.engine.value.y;
        }

        if (cursors.left.isDown || cursors.right.isDown) {
            revEngine();
        } else {
            quietEngine();
        }

        if (cursors.up.isDown) {
            sfxBoosters.volume = 0.25;
        } else {
            sfxBoosters.volume = 0;
        }

        if (!cursors.up.isDown) {
            boosters[0].visible = false;
            boosters[1].visible = false;
            boosters[0].animations.stop(boosterAnims[boosterAnimIndex], 30, true);
            boosters[1].animations.stop(boosterAnims[boosterAnimIndex], 30, true);
        }

        minerTileDissolveAnimGroup = game.add.group();
        minerTileDissolveAnimGroup.add(miner);

    },

    render: function() {
        // game.debug.bodyInfo(miner, 16, 24);
        // game.debug.cameraInfo(game.camera, 32, 32);
        // game.debug.body(miner);
        // game.debug.body(portalBaseCollision);
        // game.debug.body(groundLayer);
    }

}

function revEngine() {
    if (sfxEngineRunning.volume < 0.1) {
        sfxEngineRunning.volume += 0.001;
    } else {
        sfxEngineRunning.volume = 0.1;
    }
}

function quietEngine() {
    if (sfxEngineRunning.volume > 0.05) {
        sfxEngineRunning.volume -= 0.001;
    } else {
        sfxEngineRunning.volume = 0.05;
    }
}


// function drawGround() {

//     ground = game.add.group();
//     groundArray = [];
//     var tile;
//     for(var i = 0; i < game.world.height / groundSize; i++) {
//         groundArray[i] = [];
//         for (var j = 0; j < game.world.width / groundSize; j++) {
//             tile = game.add.sprite(i*oreSize, (2*oreSize)+(j*oreSize), 'oreSpriteSheet');


//             var oreIndex = pickOre();
//             tile.frame = oreIndex;
//             tile.resource = ores[oreIndex];

//             game.physics.enable(tile, Phaser.Physics.ARCADE);
//             tile.body.immovable = true;
//             tile.body.allowGravity = false;
//             tile.body.onCollide = new Phaser.Signal();
//             tile.body.onCollide.add(touchingGround, this);
//             groundArray[i][j] = tile;
//             ground.add(tile);
//         }
//     }

//     console.log(ground);

// }

function drawGround() {
    var data = '';

    for (var depth = 0; depth < WORLD_TILE_HEIGHT; depth++) {
        for (var x = 0; x < XTILEWIDTH; x++) {

            var ironChance = getOreChanceByName("iron");
            if (depth > 50 && ironChance - 0.1 >= 5) {
                setOreChanceByName("iron", ironChance - 0.1);
            }

            var copperChance = getOreChanceByName("copper");
            if (depth > 150 && copperChance - 0.1 >= 5) {
                setOreChanceByName("copper", copperChance - 0.1);
            }

            if (depth === 0) {
                data += tileNamesToIndices.grass; // grass tile on topsoil
            } else if (depth > 0) {
                data += pickOre(depth).toString(); //pick a random ore or dirt
            }


            if (x < XTILEWIDTH - 1) {
                data += ',';
            }

        }

        if (depth < WORLD_TILE_HEIGHT - 1) {
            data += "\n";
        }

    }
    console.log(oreChances);
    console.log(data);


    return data;
}


var oreChances = [
                    {ore: 'grunium', value: 1 },
                    {ore: 'vibranium', value: 2 },
                    {ore: 'gold', value: 5 },
                    {ore: 'lava', value: 5},
                    {ore: 'silver', value: 8 },
                    {ore: 'empty', value: 35 },
                    {ore: 'copper', value: 35 },
                    {ore: 'iron', value: 45 },
                    {ore: 'dirt', value: 300 },
                ];

function getOreChanceByName(oreName) {
    for (var i in oreChances) {
        if (oreChances[i].ore === oreName) {
            return oreChances[i].value;
        }
    }
    return null;
}

function setOreChanceByName(oreName, newValue) {
    for (var i in oreChances) {
        if (oreChances[i].ore === oreName) {
            oreChances[i].value = newValue;
            return;
        }
    }
}

// Object.prototype.getKey = function(value) {
//     for (var key in this) {
//         if (this[key] === value) {
//             return key;
//         }
//     }
//     return null;
// };


// Maybe change the depth limiting thing to just have things set to 0 probability at 0 depth then
// when the algorithm reaches the correct depth just increase the chance for that ore to a non zero value.

function pickOre(depth) {

    var num = Math.random();
    var chances = normalise(getChanceArray());
    for (var i in oreChances) {
        var cumulativeProb = sumArrayToIndex(chances, i);
        if (num < cumulativeProb) {

            // if (oreChances[i].ore === 'lava' && depth < 200) {
            //     continue;
            // }

            // if (oreChances[i].ore === 'vibranium' && depth < 50) {
            //     continue;
            // }

            // if (oreChances[i].ore === 'grunium' && depth < 100) {
            //     continue;
            // }

            if (oreChances[i].ore === "gold" && depth < 20) {
                continue;
            }
            if (oreChances[i].ore === "silver" && depth < 10) {
                continue;
            }
            return tileNamesToIndices[oreChances[i].ore];
        }
    }

    // if (num <= 10) {
    //     return 6; //backgroundDirt
    // } else if (num <= 60) {
    //     return 0; //dirt
    // } else if (num <= 77) {
    //     return 1; //iron
    // } else if (num <= 85) {
    //     return 2; //copper
    // } else if (num <= 95) {
    //     return 3; //silver
    // } else if (num <= 100) {
    //     return 4; //gold
    // }
}

function touchingGround(miner, tile) {

    if (miner.body.blocked.down && miner.body.velocity.y < -120) {
        updateHealth(-20, deathReasons.falling);
    }

    // Digging code
    if (tile.index != -1) {

        var dug = false;
        var drillDigTime = currentUpgrades.drill.value;

        // Digging right
        if (cursors.right.isDown && miner.body.blocked.right && tile.left >= miner.right && tile.bottom === miner.bottom) {
            var digTween = game.add.tween(miner).to({ x: tile.worldX + oreSize / 2, y: tile.worldY + 45}, drillDigTime, Phaser.Easing.Quintic.In, true);
            digTween.onComplete.add(killTile, this);

            miner.body.y -= 5;

            miner.body.allowGravity = false;

            enableDrill("right");
            miner.body.checkCollision.right = false;
            miner.body.checkCollision.down = false;
            digTween.start();
            dug = true;
        // Digging left
        } else if (cursors.left.isDown && miner.body.blocked.left && tile.right <= miner.left && tile.bottom === miner.bottom) {
            var digTween = game.add.tween(miner).to({ x: tile.worldX + oreSize / 2, y: tile.worldY + 45}, drillDigTime, Phaser.Easing.Quintic.In, true);
            digTween.onComplete.add(killTile, this);

            miner.body.y -= 5;

            miner.body.allowGravity = false;

            enableDrill("left");
            miner.body.checkCollision.left = false;
            miner.body.checkCollision.down = false;
            digTween.start();
            dug = true;
        // Digging down
        } else if (cursors.down.isDown && miner.body.blocked.down) {
            var digTween = game.add.tween(miner).to({ x: tile.worldX + oreSize / 2, y: tile.worldY + 45}, drillDigTime, Phaser.Easing.Quintic.In, true);
            digTween.onComplete.add(killTile, this);

            miner.body.allowGravity = false;

            enableDrill("down");

            miner.body.checkCollision.down = false;
            miner.body.checkCollision.right = false;
            miner.body.checkCollision.left = false;
            digTween.start();
            dug = true;
        }

        if (dug) {
            map.removeTile(tile.x, tile.y);
            map.putTile(tileNamesToIndices.empty, tile.x, tile.y, groundLayer);

            var spriteTile = game.add.sprite(tile.worldX + (tile.width/2), tile.worldY + (tile.width/2), "oreSpriteSheet");
            spriteTile.anchor.setTo(0.5,0.5);
            spriteTile.frame = tile.index;
            var tileDissolveTween = game.add.tween(spriteTile.scale).to({ x: 0, y: 0}, drillDigTime, Phaser.Easing.Quintic.In, true);
            minerTileDissolveAnimGroup.add(spriteTile);
            minerTileDissolveAnimGroup.sort();
            tileDissolveTween.onComplete.add( function(sprite) { return function() { sprite.destroy(); } } );
            tileDissolveTween.start();    
        }
        
    }


    function killTile () {
        miner.x = tile.worldX + oreSize / 2;
        miner.y = tile.worldY + 45;
        miner.body.velocity.x = 0;
        miner.body.velocity.y = 0;

        if (tile.index != tileNamesToIndices.dirt && tile.index != tileNamesToIndices.grass && tile.index != tileNamesToIndices.lava) {
            if (!bankDetector.alive) {
                bankDetector.revive();
            }
            addToInventory(tile.index);
        } else if (tile.index === tileNamesToIndices.lava) {
            updateHealth(LAVA_BASE_DAMAGE * currentUpgrades.radiator.value, deathReasons.lava);
        }

        miner.body.allowGravity = true;
        miner.body.checkCollision.down = true;
        miner.body.checkCollision.left = true;
        miner.body.checkCollision.right = true;
        drill.visible = false;
    }

}

function enableDrill(direction) {

    drill.visible = true;
    drill.x = drillPositions[direction].x - 1;
    drill.y = drillPositions[direction].y - 1;
    drill.angle = drillPositions[direction].angle;
    drill.animations.play( drillExtendAnims[drillAnimIndex]);
    // console.log(drillExtendAnims[drillAnimIndex]);

    if (direction === "down") {
        var drillShakeTween = game.add.tween(drill).to({ x: drill.x + 3, y: drill.y}, 50, Phaser.Easing.Bounce.Out, true, 0, -1, true);    
    } else if (direction === "right") {
        var drillShakeTween = game.add.tween(drill).to({ x: drill.x, y: drill.y + 3}, 50, Phaser.Easing.Bounce.Out, true, 0, -1, true);
    } else if (direction === "left") {
        var drillShakeTween = game.add.tween(drill).to({ x: drill.x, y: drill.y + 3}, 50, Phaser.Easing.Bounce.Out, true, 0, -1, true);
    }

    digEmitter.x = miner.x + drillPositions[direction].x * 1.3;
    digEmitter.y = miner.y + drillPositions[direction].y * 1.6;

    digEmitter.start(false, 2000, 0.2, 30);

    sfxDrill.key = sfxDrillKeys[Math.floor(Math.random() * sfxDrillKeys.length)];
    playSound(sfxDrill);
}

function normalise(arr) {
    var total = sumArrayToIndex(arr, arr.length-1);

    var normalised = [];
    for (i in arr) {
        normalised.push(arr[i] / total);
    }
    return normalised;
}

function getChanceArray() {
    chances = [];
    for (var i in oreChances) {
        chances.push(oreChances[i].value);
    }
    return chances;
}

// function getChanceOreNameArray() {
//     chances = [];
//     for (var i in oreChances) {
//         chances.push(i);
//     }
//     return chances;
// }

function sumArrayToIndex(arr, index) {
    var sum = 0;
    if (index === arr.length) {
        console.log("Index too large for sumArrayToIndex");
        return 1;
    }
    for (var i = 0; i <= index; i++) {
        sum += arr[i];
    }
    return sum;
}



function addToInventory(resourceIndex) {
    var itemsHeld = getMapSum(inventory);
    if (itemsHeld < currentUpgrades.cargoBay.value) {
        inventory[tileIndicesToNames[resourceIndex]] += 1;
        inventoryUI.text = getInventoryUIString();
    } else {
        console.log("Not enough inventory space, wasted an ore.");
    }
}

var oresInvolved = [];

function sellResources() {
    oresInvolved = [];
    bankDetector.kill();
    var portalOpenTween = game.add.tween(bankPortal.scale).to({x:1, y:1}, 300, Phaser.Easing.Quadratic.In, true, 0, 0, false);
    bankPortalEmitter.on = true;
    bankPortalEmitter.start(false, 500, 10);
    bankPortal.visible = true;
    bankPortal.animations.play('bankPortalAnim', 30, true);
    for (var item in inventory) {
        if (inventory[item] != 0) {
            var em = bankEmitters[item];
            em.emitter.x = miner.x;
            em.emitter.y = miner.y;

            em.emitter.start(true, 60000, 200, inventory[item]);

            // em.timer.destroy();

            em.timer = game.time.create();
            // console.log(em.timer);
            em.event = em.timer.add(Phaser.Timer.SECOND*oreNamesToIndices[item]*0.5, buildCallBack(em, item), this);
            oresInvolved.push(item);


            // Start the timer
            em.timer.start();
        }
        inventory[item] = 0;
    }

    inventoryUI.text = getInventoryUIString();
}

function buildCallBack(em, oreType) {
    return function () {
        suckEmitter(em, oreType);
    };
}

function suckEmitter(em, oreType) {
    em.emitter.forEach(suckResource, this, true, oreType);
}

function suckResource(particle, oreType) {
    var tw = game.add.tween(particle).to({ x: bankDetector.x, y: bankDetector.y}, 600, Phaser.Easing.Quartic.In, true, 0, 0, false);
    ts = game.add.tween(particle.scale).to({ x: 0.2, y: 0.2}, 600, Phaser.Easing.Quartic.In, true, 0, 0, false);
    tw.onComplete.add( getOreMoney, {ore: oreType});
    tw.start();
}

function getOreMoney(particle) {
    particle.kill();
    // var money = game.add.sprite(particle.x + (100 * Math.random() < 0.5 ? -1 : 1), particle.y + (100 * Math.random() < 0.5 ? -1 : 1), 'moneySign');
    var money = game.add.sprite(bankPortal.x, bankPortal.y, 'moneySign');
    money.anchor.setTo(0.5,0.5);
    var moneyTween = game.add.tween(money).to({alpha: 0}, 500, Phaser.Easing.Linear.In, true, 0, 0, false);
    moneyTween.onComplete.add(function () { money.kill();}, this);

    sellResource(this.ore);

    for (em in bankEmitters) {
        if (oresInvolved.indexOf(em) != -1 && !bankEmitters[em].timer.expired) {
            // console.log(bankEmitters[em]);
            return;
        }
    }

    var portalCloseTween = game.add.tween(bankPortal.scale).to({x:0, y:0}, 500, Phaser.Easing.Quadratic.In, true, 0, 0, false);
    portalCloseTween.onComplete.add(function() { bankPortal.visible = false; bankPortalEmitter.on = false;});
}

function sellResource(oreType) {
    console.log(oreType);
    addMoney(orePriceDict[oreType]);
}

function addMoney(value) {
    if (money + value <= 0) {
        money = 0;
    } else {
        money += value;
    }
    moneyUI.text = getMoneyUIString();
}

function updateHealth(value, reason) {
    deathReason = reason;
    var oldHealth = health;
    if (health + value <= 0) {
        health = 0;
        playerDie();
    } else if (health + value <= maxHealth) {
        health += value;
    }
    // If the old health value was larger i.e. we lost health, show the damage effects on screen.
    if (oldHealth > health) {
        damageVignette.alpha = 1;
        damageVignetteFade.start();
        playSound(sfxMinerHit);
    }
    healthUI.text = getHealthUIString();
}

function updateFuel() {
    if (fuel - 1 >= 0) {
        fuel -= 1;
    } else {
        fuel = 0;
        deathReason = deathReasons.fuel;
        playerDie();
    }
    fuelUI.text = getFuelUIString();
}

function refuel(value) {
    fuelTimer.destroy();
    fuel = fuel+value < maxFuel ? fuel+value : maxFuel;
    fuelTimer = game.time.create();
    timerEvent = fuelTimer.loop(Phaser.Timer.SECOND, updateFuel, this);
    fuelTimer.start();
    fuelUI.text = getFuelUIString();
    console.log("REFUELLED");
}

function repairFull() {
    health = maxHealth;
    healthUI.text = getHealthUIString();
    console.log("REPAIRED");
}

function buyFuel(sprite) {
    if (money >= sprite.fuelCost && fuel < maxFuel) {
        pay(sprite.fuelCost);
        refuel(sprite.fuelCost * 4);
    }
}

function preventFuelDrain() {
    fuelTimer.pause();
}

function buyRepair(sprite) {
    if (money >= sprite.repairCost && health < maxHealth) {
        pay(sprite.repairCost);
        updateHealth(sprite.repairCost);
    }
}

function pay(value) {
    money = (money - value) > 0 ? (money - value) : 0;
    moneyUI.text = getMoneyUIString();
}


//To blow up tiles surrounding the miner,
// we need to determine the tiles around the miner for some radius.
// set those tiles to background and recalculate collision.
function useDynamite() {
    var tilesAffected = getTilesAroundSprite(miner, 1);
    for (var i = 0; i < tilesAffected.length; i++) {
        var tile = map.getTile(tilesAffected[i][0], tilesAffected[i][1]);
        map.removeTile(tile.x, tile.y);
        map.putTile(tileNamesToIndices.empty, tile.x, tile.y, groundLayer);
    }
}


function playerDie() {
    explosion.x = miner.x;
    explosion.y = miner.y;
    // explosion.anchor.setTo(0.5,0.5);
    // explosion.scale = 1;
    failUIText.text = deathReason;
    failUIText.visible = true;

    sfxEngineRunning.stop();
    playSound(sfxDeath);

    explosionTween.start();
    fuelTimer.stop();
    miner.kill();
}

function restart() {
    failUIText.visible = false;
    miner.x = game.world.width / 4;
    miner.y = -200;

    playSound(sfxEngineStart);
    playSound(sfxEngineRunning);

    setMaxHealth();

    maxFuel = currentUpgrades["fuelTank"].value;
    fuel = maxFuel;
    fuelUI.text = getFuelUIString();

    money = 5;
    moneyUI.text = getMoneyUIString();

    inventorySize = currentUpgrades["cargoBay"].value;
    inventory = getBlankInventory();
    console.log(inventory);
    inventoryUI.text = getInventoryUIString();

    miner.revive();
}

function setMaxHealth() {
    maxHealth = currentUpgrades["hull"].value;
    health = maxHealth;
    healthUI.text = getHealthUIString();
}

// Only add ores to inventory not other tiles.
function getBlankInventory() {
    var inv = {};
    for (var i in oreIndicesToNames) {
        inv[oreIndicesToNames[i]] = 0;
    }
    return inv;
}

function getHealthUIString() {
    return health + "/" + maxHealth;
}

function getFuelUIString() {
    return fuel + "/" + maxFuel;
}

function getMoneyUIString() {
    return "£" + money;
}

function getInventoryUIString() {
    var invString = "";
    for (var item in inventory) {
        invString += item + ": " + inventory[item] + "\n";
    }
    var invCount = getMapSum(inventory);
    var percentageFull =  ((invCount / inventorySize) * 100).toFixed(1);
    invString += invCount + "/" + inventorySize + " (" + percentageFull + "%)";
    return invString;
}

function updateShadowTexture() {
    // This function updates the shadow texture (this.shadowTexture).
    // First, it fills the entire texture with a dark shadow color.
    // Then it draws a white circle centered on the pointer position.
    // Because the texture is drawn to the screen using the MULTIPLY
    // blend mode, the dark areas of the texture make all of the colors
    // underneath it darker, while the white area is unaffected.

    if (miner.y < 500) {
        lightSprite.visible = false;
        shadowTexture.context.fillStyle = 'rgb(100, 100, 160)';
    } else if (miner.y < 700) {
        lightSprite.visible = true;
        shadowTexture.context.fillStyle = 'rgb(200, 200, 260)';
    } else if (miner.y < 900) {
        shadowTexture.context.fillStyle = 'rgb(170, 170, 230)';
    } else if (miner.y < 1100) {
        shadowTexture.context.fillStyle = 'rgb(140, 140, 200)';
    } else if (miner.y < 1300) {
        shadowTexture.context.fillStyle = 'rgb(110, 110, 170)';
    } else if (miner.y < 1500) {
        shadowTexture.context.fillStyle = 'rgb(80, 80, 140)';
    } else if (miner.y < 1700) {
        shadowTexture.context.fillStyle = 'rgb(50, 50, 110)';
    } else {
        shadowTexture.context.fillStyle = 'rgb(20, 20, 80)';
    }

    lightSprite.x = miner.x - game.width / 2;
    lightSprite.y = miner.y - game.height / 2;

    // Draw shadow

    shadowTexture.context.fillRect(0, 0, game.width, game.height+1000);

    // Draw circle of light with a soft edge
    var gradient = shadowTexture.context.createRadialGradient(
        miner.worldPosition.x, miner.worldPosition.y, LIGHT_RADIUS * 0.75,
        miner.worldPosition.x, miner.worldPosition.y, LIGHT_RADIUS);
    gradient.addColorStop(0, 'rgba(255, 255, 255, 1.0)');
    gradient.addColorStop(1, 'rgba(255, 255, 255, 0.0)');

    shadowTexture.context.beginPath();
    shadowTexture.context.fillStyle = gradient;
    shadowTexture.context.arc(miner.worldPosition.x, miner.worldPosition.y,
        LIGHT_RADIUS, 0, Math.PI*2);
    shadowTexture.context.fill();

    // This just tells the engine it should update the texture cache
    shadowTexture.dirty = true;
};

