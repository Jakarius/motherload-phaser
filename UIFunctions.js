// UI related functions for Motherlode game.

var soundOn = true;

// Need to also stop all running sfx in here. Maybe keep list of all sfx objects and mute all.
function toggleSoundIcon(icon) {
    console.log(icon);
    if (soundOn) {
        icon.loadTexture('iconSoundOff');
        soundOn = false;
        game.sound.mute = true;
    } else {
        icon.loadTexture('iconSoundOn');
        soundOn = true;
        game.sound.mute = false;
    }
}

function playSound(sound) {
    if (soundOn) {
        sound.play();
    }
}

function binarySearch(arr, value) {
    var found = false;
    var high = arr.length - 1;
    var low = 0;
    var mid = high + low / 2;
    while (!found) {


        if (arr[mid] === value) {
            return mid;
        } else if (value > arr[mid]) {
            low = mid + 1;
        } else if (value < arr[mid]) {
            high = mid - 1;
        }

        mid = Math.floor((high + low) / 2);

        if (low > high) {
            return -1;
        }
    }
}

function checkOverlap(spriteA, spriteB) {

    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();

    return Phaser.Rectangle.intersects(boundsA, boundsB);

}

function getMapSum(map) {
    var count = 0;
    for (var key in map) {
        count += map[key];
    }
    return count;
}

function getTilesAroundSprite(sprite, radius) {
    var xTile = Math.floor(sprite.x / oreSize);
    var yTile = Math.floor(sprite.y / oreSize);

    tiles = [];
    for (var i = yTile - radius; i <= yTile + radius; i++) {
        for (var j = xTile - radius; j <= xTile + radius; j++) {
            // if (i != yTile && j != xTile) {
                tiles.push([j,i]);
            // }
        }
    }
    return tiles;
}