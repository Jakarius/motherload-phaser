var oreSize = 80;

var loadState = {

    preload: function () {
        assetsPath = "assets/";

        //Backgrounds
        game.load.image('skyBG', assetsPath + 'skyBG.png');
        game.load.image('dirtBG', assetsPath + 'bgTileSprite.png');
        game.load.image('grass', assetsPath + 'grass1.png');

        game.load.image('fuelStation', assetsPath + 'fuelStation.png');
        game.load.image('repairStation', assetsPath + 'repairStation.png');
        game.load.image('fuelStationMoneyIcon', assetsPath + 'fuelStationMoneyIcon.png');
        game.load.image('shopSelectionHighlighter', assetsPath + 'shopSelectionHighlighter.png');
        game.load.image('upgradeStation', assetsPath + 'upgradesBuilding.png');
        game.load.image('portalGate', assetsPath + 'portalGate.png');
        game.load.image('portalParticle', assetsPath + 'portalParticle.png');

        game.load.image('miner1', assetsPath + 'miner2.png');
        game.load.image('nullMiner', assetsPath + 'nullMiner.png');
        game.load.image('explosion1', assetsPath + 'explosion1.png');
        game.load.image('damageVignette', assetsPath + 'damageVignette.png');

        game.load.image('iconSoundOn', assetsPath + 'iconSoundOn.png');
        game.load.image('iconSoundOff', assetsPath + 'iconSoundOff.png');

        game.load.image('moneySign', assetsPath + 'moneySign.png');
        game.load.image('healthIcon', assetsPath + 'healthUIIcon.png');
        game.load.image('fuelIcon', assetsPath + 'fuelUIIcon.png');
        game.load.spritesheet('inventoryWindow', assetsPath + 'inventoryWindow.png', 174, 203, 4);

        game.load.image('dirtRoughTop', assetsPath + 'dirtTileTop.png');

        game.load.image('heli1', assetsPath + 'heli1.png');
        game.load.image('dirt', assetsPath + 'dirt2.png');
        game.load.image('dirtParticle', assetsPath + 'dirtParticle.png');
        game.load.image('minerNull', assetsPath + 'minerNull.png');
        game.load.spritesheet('oreSpriteSheet', assetsPath + 'oreSpriteSheet.png', oreSize, oreSize, 10);
        game.load.spritesheet('oreParticleSpriteSheet', assetsPath + 'oreParticleSpriteSheet.png', 22, 22, 7);
        game.load.spritesheet('boosterSpriteSheet', assetsPath + 'boosterSpriteSheet.png', 10, 17, 9);
        game.load.spritesheet('drillSpriteSheet', assetsPath + 'drillSpriteSheet.png', 52, 32, 24);
        game.load.spritesheet('hullColourSpriteSheet', assetsPath + 'hullColours.png', 66, 55, 6);
        game.load.spritesheet('portalSpriteSheet', assetsPath + 'portalSpriteSheet.png', 284, 276, 3);
        game.load.spritesheet('upgradePriceDisplay', assetsPath + 'upgradePriceDisplay.png', 55, 30, 2);
        game.load.spritesheet('engines', assetsPath + 'engines.png', 59, 36, 3);
        game.load.spritesheet('drillUpgrades', assetsPath + 'drillTextures.png', 52, 32, 6);
        game.load.spritesheet('fuelTanks', assetsPath + 'fuelTanks.png', 26, 48, 5);
        game.load.spritesheet('cargoBays', assetsPath + 'cargoBaySpriteSheet.png', 65, 41, 6);

        game.load.spritesheet('repairDroid', assetsPath + 'repairDroid.png', 52, 38, 6);

        game.load.audio('sfxDrill1', assetsPath + 'sfxDrill1.mp3');
        game.load.audio('sfxDrill2', assetsPath + 'sfxDrill2.mp3');
        game.load.audio('sfxDrill3', assetsPath + 'sfxDrill3.mp3');
        game.load.audio('sfxDrill4', assetsPath + 'sfxDrill4.mp3');
        game.load.audio('sfxMinerHit', assetsPath + 'sfxMinerHit.mp3');
        game.load.audio('sfxDeath', assetsPath + 'sfxDeath.mp3');
        game.load.audio('sfxEngineStart', assetsPath + 'sfxEngineStart.mp3');
        game.load.audio('sfxEngineRunning', assetsPath + 'sfxEngineRunning.mp3');
        game.load.audio('sfxBoosters1', assetsPath + 'sfxBoosters1.mp3');
        game.load.audio('sfxBoosters2', assetsPath + 'sfxBoosters2.mp3');
        game.load.audio('sfxBoosters3', assetsPath + 'sfxBoosters3.mp3');

        game.load.start();
    },

	create: function() {
		game.state.start('play');
	}
}