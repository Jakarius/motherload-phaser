
GAME_WIDTH = 1024;
GAME_HEIGHT = 576;

var game = new Phaser.Game(GAME_WIDTH, GAME_HEIGHT, Phaser.AUTO, 'gameDiv');

game.state.add("load", loadState);
game.state.add("play", playState);

game.state.start("load");